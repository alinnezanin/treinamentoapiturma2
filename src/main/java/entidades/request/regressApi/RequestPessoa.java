package entidades.request.regressApi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class RequestPessoa {

    @JsonProperty("job")
    private String job;
    @JsonProperty("name")
    private String name;
}
