package entidades.responses.regressApi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class ResponsePostPessoa {

    @JsonProperty("id")
    private String id;
    @JsonProperty("createdAt")
    private String createdAt;
}
