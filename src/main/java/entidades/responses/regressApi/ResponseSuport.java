package entidades.responses.regressApi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class ResponseSuport {
    @JsonProperty("url")
    private  String url;
    @JsonProperty("text")
    private String text;
}
