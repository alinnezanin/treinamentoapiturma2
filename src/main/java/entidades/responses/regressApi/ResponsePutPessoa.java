package entidades.responses.regressApi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class ResponsePutPessoa {
    @JsonProperty("updatedAt")
    private String updatedAt;
 }
