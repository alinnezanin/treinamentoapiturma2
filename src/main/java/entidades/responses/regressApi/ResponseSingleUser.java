package entidades.responses.regressApi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class ResponseSingleUser {
    @JsonProperty("data")
    private ResponseData data;
    @JsonProperty("support")
    private ResponseSuport support;
}
