package entidades.responses.apiDeTarefas;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseContactsAttributes {

    @JsonProperty("name")
    private String name;
    @JsonProperty("last-name")
    private String lastName;
    @JsonProperty("email")
    private String email;
    @JsonProperty("age")
    private int age;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("address")
    private String address;
    @JsonProperty("state")
    private String state;
    @JsonProperty("city")
    private String city;






}
