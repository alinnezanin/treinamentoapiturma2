package entidades.responses.apiDeTarefas;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class ResponseSingleContact {

    @JsonProperty("data")
    private ResponseContact data;
}
