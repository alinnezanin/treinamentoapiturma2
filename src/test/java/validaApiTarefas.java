
import org.json.JSONObject;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;
import static io.restassured.RestAssured.*;

public class validaApiTarefas {

    @Test
    public void validPost() {
        JSONObject param = new JSONObject();
        param.put("name", "Maria");
        param.put("last_name", "Freitas Silva");
        param.put("email", "abcfegth@gmail.com");
        param.put("age", "28");
        param.put("phone", "21978789575");
        param.put("address", "Rua dois");
        param.put("state", "Minas Do Leão");
        param.put("city", "Montes Claros");
        given()
                .contentType("application/json") //adicionar
                .when()
                .relaxedHTTPSValidation()
                .body(param.toString())
                .log().all()
                .post("https://api-de-tarefas.herokuapp.com/contacts")
                .then()
                .log().all()
                .statusCode(201)
                .body("data.attributes.email", is("abcfegth@gmail.com"));

    }

     /* Exercicio: Na classe ValidaApiWcaquino
        Fazer um post na api do http://restapi.wcaquino.me/users
             e validar o retorno, usar esse corpo
            {
                "name": "Silveira m",
                    "id": "12345",
                    "salary": "1000",
                    "age": "20"
            }*/

    @Test
    public void validPut() {
        JSONObject param = new JSONObject();
        param.put("name", "Mariana");
        param.put("last_name", "Freitas Silveira");
        param.put("email", "mari@ana.com");
        param.put("age", "25");
        param.put("phone", "21978789575");
        param.put("address", "Rua dois");
        param.put("state", "Minas Do Leão");
        param.put("city", "Montes Claros");
        given()
                .pathParam("id","1224")
                .contentType("application/json") //adicionar
                .when()
                .relaxedHTTPSValidation()
                .body(param.toString())
                .log().all()
                .put("https://api-de-tarefas.herokuapp.com/contacts/{id}")
                .then()
                .log().all()
                .statusCode(200)
                .body("data.attributes.email", is("mari@ana.com"));

    }

    @Test
    public  void validaDelete(){
        given()
                .pathParam("id", "1224")
                .when()
                .delete("https://api-de-tarefas.herokuapp.com/contacts/{id}")
                .then()
                .statusCode(204);
    }



}
