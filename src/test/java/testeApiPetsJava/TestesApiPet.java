package testeApiPetsJava;

import entidades.responses.apiPets.ResponsePetSinglePet;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

public class TestesApiPet {

    @Test
    public void validaGetComParametro(){
        ResponsePetSinglePet responsePetSinglePet =
        given()
                .relaxedHTTPSValidation()
                .pathParam("id", "1")
                .when()
                .log().all()
                .get("http://petstore-demo-endpoint.execute-api.com/pet/{id}")
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .body().as(ResponsePetSinglePet.class);
        Assert.assertEquals(responsePetSinglePet.getName(), "Whiskers");
        Assert.assertEquals(responsePetSinglePet.getType(), "cat");
        Assert.assertEquals(responsePetSinglePet.getPrice(), "12.99");
    }


}
