package testesRegressApiJava;

import entidades.dataprovider.DataProviderTest;
import entidades.request.regressApi.RequestPessoa;
import entidades.responses.regressApi.ResponsePutPessoa;
import entidades.responses.regressApi.ResponseSingleUser;
import entidades.responses.regressApi.ResponsePostPessoa;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static io.restassured.RestAssured.*;

public class TestesRegressApiJava {

    @Test
    public void validaGetComParametro(){
       String esperado = "To keep ReqRes free, contributions towards server costs are appreciated!";
        ResponseSingleUser response =
             given()
                     .relaxedHTTPSValidation()
                     .header("content-type","application/json")
                     .pathParam("id","2")
                .when()
                     .get("https://reqres.in/api/users/{id}")
                .then()
                     .statusCode(200)
                     .extract()
                .body().as(ResponseSingleUser.class);
        Assert.assertEquals(response.getData().getFirstName(),"Janet");
        Assert.assertEquals(response.getSupport().getText(), esperado);
    }

    @Test(dataProvider = "providerNameJob", dataProviderClass = DataProviderTest.class)
   public void validaPost(String name, String Job){
        RequestPessoa request = RequestPessoa
                .builder()
                .name(name)
                .job(Job)
                .build();

       ResponsePostPessoa response =  given()
                .body(request)
                .when()
               .log().all()
                .post("https://reqres.in/api/users")
                .then()
               .log().all()
                .statusCode(201)
                .extract()
                .body().as(ResponsePostPessoa.class);

       Date dataAtual = new Date();
       String valor  = new SimpleDateFormat("yyyy-MM-dd").format(dataAtual);
       Assert.assertTrue(response.getCreatedAt().contains(valor));
       Assert.assertNotNull(response.getId());
   }

    @Test
    public void validaPut(){
        RequestPessoa request = RequestPessoa
                .builder()
                .name("Maria")
                .job("Medica")
                .build();

        ResponsePutPessoa response =  given()
                .body(request)
                .pathParam("id", "2")
                .when()
                .log().all()
                .put("https://reqres.in/api/users/{id}")
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .body().as(ResponsePutPessoa.class);

        Date dataAtual = new Date();
        String valor  = new SimpleDateFormat("yyyy-MM-dd").format(dataAtual);
        Assert.assertTrue(response.getUpdatedAt().contains(valor));
    }


    @Test
    public void validaDelete(){
        given()
                .pathParam("id","2")
                .when()
                .delete("https://reqres.in/api/users/{id}")
                .then()
                .statusCode(204);
    }




}
