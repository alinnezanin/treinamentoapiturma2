import org.json.JSONObject;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;
import static io.restassured.RestAssured.*;


public class ValidaReqresApi {

     /*Exercicio 01 - Aula 02
    Fazer GET regressApi na classe ValidRegresApi e verificar os seguintes campos

    Elementos de lista:
    name indice 0"
    year indice 1"
    color indice 2 "
    pantone_value indice 3 "
    Elementos de segundo nível
    company
    url
    text
    Código*/



    @Test
    public void validaRegresApiListaESegundoNivel(){

        given()
                .when()
                .log().all()
                .get("https://reqres.in/api/user")
                .then()
                .log().all()
                .statusCode(200)
                .body("data[0].name",is("cerulean"))
                .body("data[1].year",is(2001))
                .body("data[2].color",is("#BF1932"))
                .body("data[3].pantone_value",is("14-4811"))
                .body("ad.company", is("StatusCode Weekly"))
                .body("ad.url", is("http://statuscode.org/"))
                .body("ad.text", is("A weekly newsletter focusing on software development, infrastructure, the server, performance, and the stack end of things."));

    }


    @Test
    public void validarPost(){
        JSONObject requestParam = new JSONObject();
        requestParam.put("name","Joana Carolina");
        requestParam.put("job","QA");
        given()
                .when()
                .body(requestParam)
                .log().all()
                .post("https://reqres.in/api/users")
                .then()
                .log().all()
                .statusCode(201)
                .body("createdAt",containsString("2020-11-07"));
    }




}
