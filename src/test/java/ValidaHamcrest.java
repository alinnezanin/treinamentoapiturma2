import org.hamcrest.Matchers;
import org.testng.annotations.Test;
import java.util.Arrays;
import java.util.List;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

public class ValidaHamcrest {

    @Test
    public void exercicioHamcrest(){
        String nome = "Aline";
        int idade = 29;
        assertThat(nome, is("Aline"));//is verifica o valor exato
        assertThat(100, greaterThan(99));
        assertThat(100, greaterThanOrEqualTo(100));
        assertThat(100, lessThan(150));
        assertThat(100, lessThanOrEqualTo(100));
        assertThat(nome, isA(String.class));  //isA verifica tipo de dado
        assertThat(idade, isA(Integer.class));

        List<Integer> listaNumeros = Arrays.asList(1,2,3,4,5,6,7,8,9,0);//criando a lista
        assertThat(listaNumeros, hasSize(10)); //verifica tamanho
        assertThat(listaNumeros, contains(1,2,3,4,5,6,7,8,9,0)); //verifica todos os valores em ordem, tem que ter todos os itens
        assertThat(listaNumeros, containsInAnyOrder(2,4,6,8,0,1,3,5,7,9)); //verifica todos os valores em qualquer ordem, tem que ter todos os itens
        assertThat(listaNumeros, hasItems(2,4,6));
        assertThat(listaNumeros, hasItem(4));



    }



}
