

import org.json.JSONObject;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;
import static io.restassured.RestAssured.*;

public class ValidaApiWcaquino {


    /*Exercício: verifique se o Maria Joaquina tem Mais de 18 e menos de 60 anos
     * Verifique se o tipo de dado do ID é inteiro
     * Verifique se o salario de Maria é maior doque 1500*/

    @Test
    public void validarGetPrimeiroNivel() {
        given()
                .when()
                .get("http://restapi.wcaquino.me/users/2")
                .then()
                .statusCode(200)
        .body("id",  is(2))
        .body("name",is("Maria Joaquina"))
        .body("age", is(25))
        .body("salary",is(2500))
                .body("age",greaterThan(18))
                .and()
                .body("age", lessThan(60))
                .body("id", isA(Integer.class))
                .body("salary", greaterThan(1500));
    }


    /****** Inicio Aula 02 *****/
    @Test
    public void validarGetSegundoNivel(){
        given()
                .when()
                .log().all()
                .get("http://restapi.wcaquino.me/users/2")
                .then()
                .log().all()
                .statusCode(200)
                .body("endereco.rua", is("Rua dos bobos"))
                .and()
                .body("endereco.numero", is(0));
    }


    @Test
    public void validaGetLista(){
        given()
                .when()
                .log().all()
                .get("http://restapi.wcaquino.me/users/3")
                .then()
                .log().all()
                .body("filhos", hasSize(2))
                .body("filhos[0].name", is("Zezinho"))
                .body("filhos[1].name", is("Luizinho"))
                .body("filhos.name", contains("Zezinho", "Luizinho"))
                .body("filhos.name", containsInAnyOrder("Luizinho", "Zezinho"));
                //.body("filhos.name", hasItem("Huguinho"));

    }

    /*Exercicio 01 - Aula 02
    Fazer GET regressApi na classe ValidRegresApi*/


    @Test
    public void validaGetListaRaiz(){
        given()
                .when()
                .log().all()
                .get("http://restapi.wcaquino.me/users/")
                .then()
                .log().all()
                .statusCode(200)
                .body("$", hasSize(3))
                .body("name", hasItems("João da Silva", "Maria Joaquina","Ana Júlia"))
                .body("name", hasItem("Ana Júlia"))
                .body("name[1]", is("Maria Joaquina"))
                .body("endereco[1].rua" ,is("Rua dos bobos"))
                .body("filhos[2].name[1]" ,is("Luizinho"));

    }

    /* Exercicio: Fazer um get na API http://petstore-demo-endpoint.execute-api.com/pets"
    * Localizar e verificar type indice 0  name indice 1 price indice 2
    * Criar execicio na classe ValidaPetStoreApi
    */

@Test
    public void validaGetComParametro(){
        given()
                .header("content-type", "aplication/json")
                .pathParam("users","users")
                .pathParam("id","2")
                .when()
                .get("http://restapi.wcaquino.me/{users}/{id}")
                .then()
                .statusCode(200)
                .body("name", is("Maria Joaquina"))
                .body("endereco.rua", is("Rua dos bobos"))
                .body(notNullValue());
    }


     /*Exercicio: Criar um  get com paramentro para a aplicação
            http://petstore-demo-endpoint.execute-api.com/pets
            na classe ValidaPetStore podem escolher qual id passar
                   e validar todos os atributos (id type name price)*/



    /* Exercicio: Na classe ValidaApiWcaquino
        Fazer um post na api do http://restapi.wcaquino.me/users
             e validar o retorno, usar esse corpo
            {
                "name": "Silveira m",
                    "id": "12345",
                    "salary": "1000",
                    "age": "20"
            }*/

    @Test
    public void validaPost(){
        JSONObject object = new JSONObject();
        object.put("name","Zanin A.");
        object.put("id", "2");
        object.put("salary", "10000");
        object.put("age", "35");
        given()
                .contentType("application/json")
                .body(object.toString())
                .when()
                .relaxedHTTPSValidation()
                .post("http://restapi.wcaquino.me/users")
                .then()
                .statusCode(201)
                .body("name",is("Zanin A."))
                .body("salary", is("10000"))
                .body("age", is("35"));
    }


    @Test
    public void validaPut(){
        JSONObject object = new JSONObject();
        object.put("name","Silveira A.");
        object.put("salary", "15000");
        object.put("age", "40");
        given()
                .header("content-type","application/Json")
                .pathParam("id", "1")
                .when()
                .relaxedHTTPSValidation()
                .body(object.toString())
                .log().all()
                .put("http://restapi.wcaquino.me/users/{id}")
                .then()
                .log().all()
                .statusCode(200)//200 porque a aplicação responde 200 contudo o padrão seria 201
                .body("name",is("Silveira A."))
                .body("salary", is("15000"))
                .body("age", is("40"));

    }

// ver como dar put em segundo nível
    @Test
    public void validaPutJsonSegundoNivel(){
        JSONObject object = new JSONObject();
        object.put("name","Silva A.");
        object.put("rua", "Rua dos bocó");
        object.put("age", "35");
        object.put("salary", "15000");
        given()
                .header("content-type","application/Json")
                .pathParam("id", "2")
                .when()
                .relaxedHTTPSValidation()
                .body(object.toString())
                .log().all()
                .put("http://restapi.wcaquino.me/users/{id}")
                .then()
                .log().all()
                .statusCode(200)//200 porque a aplicação responde 200 contudo o padrão seria 201
                .body("name",is("Silva A."))
                .body("endereco.rua", is("Rua dos bocó"));

    }


    /*Exercicio: criar um método PUT no api-de-tarefas alterando o contado já criado */

    @Test
    public void validarDelete(){
        given()
                .when()
                .pathParam("id","1" )
                .delete("http://restapi.wcaquino.me/users/{id}")
                .then()
                .statusCode(204);
    }

    /*Exercicio: criar um método DELETE no api-de-tarefas */

}
