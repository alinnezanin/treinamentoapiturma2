import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;
import static io.restassured.RestAssured.*;

public class ValidaPetstoreApi {


    /* Exercicio: Fazer um get na API http://petstore-demo-endpoint.execute-api.com/pets"
     * Localizar e verificar type indice 0  name indice 1 price indice 2
     * Criar execicio na classe ValidaPetStoreApi
     */

    @Test
    public void validaGetListaRaiz(){
        given()
                .when()
                .log().all()
                .get("http://petstore-demo-endpoint.execute-api.com/pets")
                .then()
                .log().all()
                .statusCode(200)
                .body("type[0]",is("dog"))
                .body("name[1]",is("Max"))
                .body("price[2]",is(1200));
    }

    /*Exercicio: Criar um  get com paramentro para a aplicação
    http://petstore-demo-endpoint.execute-api.com/pets
    na classe ValidaPetStore podem escolher qual id passar
            e validar todos os atributos (id type name price)*/
        @Test
        public void validaGetComParametro(){
           given()
                   //.header("content-type", "application/json")
                   .pathParam("animal", "pet")
                   .pathParam("id", "2")
                   .when()
                   .log().all()
                   .get(" http://petstore-demo-endpoint.execute-api.com/{animal}/{id}")
                   .then()
                   .log().all()
                   .statusCode(200)
                   .body("type", is("cat"))
                   .body("name", is("Whiskers"))
                   .body("price", is(Float.parseFloat("12.99")));
        }



}
