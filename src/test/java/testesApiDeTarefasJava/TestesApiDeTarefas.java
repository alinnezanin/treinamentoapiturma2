package testesApiDeTarefasJava;

import entidades.request.apiDeTarefas.RequestContact;
import entidades.responses.apiDeTarefas.ResponseSingleContact;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

public class TestesApiDeTarefas {


    @Test
    public void validaGetComParametroJava(){

        ResponseSingleContact response =
                    given()
                            .relaxedHTTPSValidation()
                            .header("content-type", "application/json")
                            .pathParam("id","24")
                            .when()
                            .get("https://api-de-tarefas.herokuapp.com/contacts/{id}")
                            .then()
                            .statusCode(200)
                            .extract()
                            .body().as(ResponseSingleContact.class);

        Assert.assertEquals(response.getData().getResponseContactsAttributes().getName(), "Pedro");
        Assert.assertEquals(response.getData().getResponseContactsAttributes().getEmail(),"apfariaD@gmail.com");
        Assert.assertEquals(response.getData().getResponseContactsAttributes().getAge(),28);
        Assert.assertEquals(response.getData().getType(),"contacts");


    }

    /*exercício para aula dia 21: Criar um teste com POST e um com PUT para
     * a API-DE-TAREFAS, se possivel udar dataprovider*/


    @Test
    public void validaPostApiDeTarefas(){

        RequestContact requestContact =
                RequestContact.builder()
                .name("Aline")
                .lastName("Zanin")
                .email("1515156@dbserver.com.br")
                .age("58")
                .phone("998848484")
                .address("rua xxx nro y")
                .state("RS")
                .city("POA").build();

        ResponseSingleContact response =
                given()
                        .relaxedHTTPSValidation()
                        .header("content-type", "application/json")
                        .body(requestContact)
                        .when()
                        .log().all()
                        .post("https://api-de-tarefas.herokuapp.com/contacts/")
                        .then()
                        .log().all()
                        .statusCode(201)
                        .extract()
                        .body().as(ResponseSingleContact.class);

        Assert.assertEquals(response.getData().getResponseContactsAttributes().getName(), "Aline");
        Assert.assertEquals(response.getData().getResponseContactsAttributes().getEmail(),"1515156@dbserver.com.br");
        Assert.assertEquals(response.getData().getResponseContactsAttributes().getAge(),58);
        Assert.assertEquals(response.getData().getType(),"contacts");

    }

    @Test
    public void validaPutApiDeTarefas(){

        RequestContact requestContact =
                RequestContact.builder()
                        .name("Aline Maria")
                        .lastName("Freitas")
                        .email("23456@dbserver.com.br")
                        .age("58")
                        .phone("998848484")
                        .address("rua xxx nro y")
                        .state("RS")
                        .city("POA").build();

        ResponseSingleContact response =
                given()
                        .relaxedHTTPSValidation()
                        .header("content-type", "application/json")
                        .pathParam("id","24")
                        .body(requestContact)
                        .when()
                        .log().all()
                        .put("https://api-de-tarefas.herokuapp.com/contacts/{id}")
                        .then()
                        .log().all()
                        .statusCode(200)
                        .extract()
                        .body().as(ResponseSingleContact.class);

        Assert.assertEquals(response.getData().getResponseContactsAttributes().getName(), "Aline Maria");
        Assert.assertEquals(response.getData().getResponseContactsAttributes().getEmail(),"23456@dbserver.com.br");
        Assert.assertEquals(response.getData().getResponseContactsAttributes().getAge(),58);
        Assert.assertEquals(response.getData().getType(),"contacts");

    }







}
